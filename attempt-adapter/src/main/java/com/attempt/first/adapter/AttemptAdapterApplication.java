package com.attempt.first.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttemptAdapterApplication{
	public static void main(String[] args) {
		SpringApplication.run(AttemptAdapterApplication.class, args);
	}
}
